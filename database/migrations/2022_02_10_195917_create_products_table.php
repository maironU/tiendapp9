<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('size_id')->references('id')->on('sizes')->onDelete('cascade')->onUpdate('cascade');
            $table->longText('observations');
            $table->foreignId('mark_id')->references('id')->on('marks')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('quantity');
            $table->date('boarding_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
