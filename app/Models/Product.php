<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "size_id",
        "mark_id",
        "quantity",
        "boarding_date",
        "observations",
    ];

    public function mark(){
        return $this->belongsTo(Mark::class);
    }

    public function size(){
        return $this->belongsTo(Size::class);
    }
}
