<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mark;
use App\Http\Requests\MarkRequest;
use Illuminate\Support\Facades\DB;

class MarkController extends Controller
{
    private $mark;

    public function __construct(Mark $mark)
    {
        $this->mark = $mark;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marks = $this->mark->get();
        return view('marks.index', compact('marks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarkRequest $request)
    {
        try{
            DB::beginTransaction();
            $this->mark::create($request->all());

            DB::commit();
            return redirect()->back()->with('created', 'Marca creada correctamente');
        }catch(\Exception $e){
            DB::rollback();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mark $mark)
    {
        return view('marks.edit', compact('mark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mark $mark)
    {
        try{
            DB::beginTransaction();
            $data = $request->only('name', 'reference');
            $mark->update($data);

            DB::commit();
            return redirect()->back()->with('created', 'Marca actualizada correctamente');
        }catch(\Exception $e){
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mark $mark)
    {
        $mark->delete();
        return redirect('/marks')->with('created', 'Marca eliminada correctamente');
    }
}
