<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"              => "bail|required|string",
            "size_id"           => "bail|required|integer|exists:sizes,id",
            "mark_id"           => "bail|required|integer|exists:marks,id",
            "observations"      => "bail|required|string",
            "quantity"          => "bail|required|integer",
            "boarding_date"     => "bail|required|string"
        ];
    }

    public function messages(){
        return  [
            "name.required"                  => "Ingrese Nombre del producto",
            "size_id.required"               => "Ingrese talla del producto",
            "mark_id.required"               => "Escoja la marca del producto",
            "quantity.required"              => "Ingrese la cantidad",
            "boarding_date.required"         => "Escoja la fecha de embarque",
            "observations.required"          => "Ingrese las observaciones",
        ];
    }
}
