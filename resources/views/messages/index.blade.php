@if(session()->has('created'))
    <div class="alert alert-success alert-dismissible w-100">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ session()->get('created') }}
    </div>
@endif
