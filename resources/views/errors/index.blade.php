@if($errors->any())
    <div class="alert alert-dismissible fade show mx-auto w-100" style="background: #E05764" role="alert">
        <div class="d-flex flex-column">
            @foreach($errors->all() as $error)
                <li class="text-white">{{$error}}</li>
            @endforeach
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
