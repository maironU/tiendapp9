<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <title>Document</title>
    </head>
    <body>
        @include('layouts/header')

        <div class="mt-4">
            <h1 class="mb-5 text-center w-100">
                @yield('title')
            </h1>
            @yield('content')
        </div>

        @include('layouts/scripts')
    </body>
</html>
