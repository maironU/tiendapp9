@extends('layouts/app')

@section('title') Crear Marca @endsection

@section('content')
    <div class="col-12 col-sm-10 col-md-8 mx-auto row m-0" style="max-width: 600px">
        @include('errors.index')
        @include('messages.index')
        <form action="/marks" method="POST" class="row m-0 w-100">
            @csrf
            <div class="form-group mb-3 col-6 pl-0">
                <label for="">Nombre de la marca</label>
                <input required type="text" name="name" class="form-control" placeholder="Ingrese nombre" value={{ old('name') }}>
            </div>
            <div class="form-group mb-2 col-6 pr-0">
                <label for="">Referencia</label>
                <input required type="text" name="reference" class="form-control" placeholder="Ingrese referencia" value={{ old('reference') }}>
            </div>
            <button class="btn btn-block btn-success">Crear</button>
        </form>
    </div>
@endsection
