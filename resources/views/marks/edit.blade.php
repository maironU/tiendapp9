@extends('layouts/app')

@section('title') Editar Marca @endsection

@section('content')
    <div class="col-12 col-sm-10 col-md-8 mx-auto row m-0" style="max-width: 600px">
        @include('errors.index')
        @include('messages.index')
        <form action="{{ url("marks/{$mark->id}") }}" method="POST" class="row m-0 w-100">
            @csrf
            @method('PUT')
            <div class="form-group mb-3 col-6 pl-0">
                <label for="">Nombre de la marca</label>
                <input type="text" name="name" class="form-control" placeholder="Ingrese nombre" value="{{ $mark->name }}">
            </div>
            <div class="form-group mb-2 col-6 pr-0">
                <label for="">Referencia</label>
                <input type="text" name="reference" class="form-control" placeholder="Ingrese referencia" value="{{ $mark->reference }}">
            </div>
            <button class="btn btn-block btn-success">Actualizar</button>
        </form>
    </div>
@endsection
