@extends('layouts/app')

@section('title') Marcas @endsection

@section('content')
    <div class="row container mx-auto">
        @include('errors.index')
        @include('messages.index')
        @if(count($marks) > 0)
            @foreach($marks as $mark)
                <div class="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 p-2">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $mark->name }}</h5>
                            <p class="card-text">Referencia {{ $mark->reference }}</p>
                            <a href="{{ url("marks/{$mark->id}/edit") }}" class="btn btn-primary btn-block">Editar</a>
                            <form action="{{ url("marks/{$mark->id}") }}" method="POST" class="mt-2">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">Eliminar</a>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div>No hay marcas <a href="/marks/create">Crear</a></div>
        @endif
    </div>
@endsection
