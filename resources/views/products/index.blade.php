@extends('layouts/app')

@section('title') Productos @endsection

@section('content')
    <div class="row container mx-auto">
        @include('errors.index')
        @include('messages.index')

        @if(count($products) > 0)
            @foreach($products as $product)
                <div class="col-12 col-md-6 col-lg-6 col-xl-4 p-2">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <ul>
                                <li>Talla: {{ $product->size->name }}</li>
                                <li>Marca: {{ $product->mark->name }}</li>
                                <li>Cantidad: {{ $product->quantity }}</li>
                                <li>Fecha embarque: {{ $product->boarding_date }}</li>
                            </ul>
                            <p class="card-text">{{ $product->observations }}</p>
                            <a href="{{ url("products/{$product->id}/edit") }}" class="btn btn-primary btn-block">Editar</a>
                            <form action="{{ url("products/{$product->id}") }}" method="POST" class="mt-2">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">Eliminar</a>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div>No hay productos <a href="/products/create">Crear</a></div>
        @endif
    </div>
@endsection
