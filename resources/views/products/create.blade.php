@extends('layouts/app')

@section('title') Crear Producto @endsection

@section('content')
    <div class="col-12 col-sm-10 col-md-8 mx-auto row m-0" style="max-width: 600px">
        @include('errors.index')
        @include('messages.index')
        <form action="/products" method="POST" class="row m-0 w-100">
            @csrf
            <div class="form-group mb-3 col-12 col-sm-6">
                <label for="name">Nombre del producto</label>
                <input required type="text" name="name" class="form-control" placeholder="Ingrese nombre" value="{{ old('name') }}">
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="size_id">Talla</label>
                <select required name="size_id" class="form-control">
                    <option value="">Seleccionar</option>
                    @foreach($sizes as $size)
                        <option @if (old('size_id') == $size->id) selected="selected" @endif value="{{ $size->id }}">{{ $size->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="mark_id">Marca del producto</label>
                <select required name="mark_id" class="form-control">
                    <option value="">Seleccionar</option>
                    @foreach($marks as $mark)
                        <option @if (old('mark_id') == $mark->id) selected="selected" @endif value="{{ $mark->id }}">{{ $mark->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="quantity">Cantidad inventario</label>
                <input required type="number" name="quantity" class="form-control" placeholder="Ingrese la cantidad" value="{{ old('quantity') }}">
            </div>
            <div class="form-group mb-2 col-12">
                <label for="boarding_date">Fecha de embarque</label>
                <input required type="date" name="boarding_date" class="form-control" value="{{ old('boarding_date') }}">
            </div>
            <div class="form-group mb-2 col-12">
                <label for="observations">Observaciones</label>
                <textarea required name="observations" rows="5" cols="20" class="form-control" placeholder="Ingrese observaciones" value="{{ old('observations') }}"></textarea>
            </div>
            <button class="btn btn-block btn-success mx-3">Crear</button>
        </form>
    </div>
@endsection
