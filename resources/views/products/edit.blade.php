@extends('layouts/app')

@section('title') Editar Producto @endsection

@section('content')
    <div class="col-12 col-sm-10 col-md-8 mx-auto row m-0" style="max-width: 600px">
        @include('errors.index')
        @include('messages.index')
        <form action="{{ url("products/{$product->id}") }}" method="POST" class="row m-0 w-100">
            @csrf
            @method('PUT')
            <div class="form-group mb-3 col-12 col-sm-6">
                <label for="name">Nombre del producto</label>
                <input type="text" name="name" class="form-control" placeholder="Ingrese nombre" value="{{ $product->name }}">
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="size_id">Talla</label>
                <select name="size_id" class="form-control">
                    <option value="">Seleccionar</option>
                    @foreach($sizes as $size)
                        @if($size->id == $product->size_id)
                            <option selected value="{{ $size->id }}">{{ $size->name }}</option>
                        @else
                            <option value="{{ $size->id }}">{{ $size->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="mark_id">Marca del producto</label>
                <select name="mark_id" class="form-control">
                    <option value="">Seleccionar</option>
                    @foreach($marks as $mark)
                        @if($mark->id == $product->mark_id)
                            <option selected value="{{ $mark->id }}">{{ $mark->name }}</option>
                        @else
                            <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 col-12 col-sm-6">
                <label for="quantity">Cantidad inventario</label>
                <input type="text" name="quantity" class="form-control" placeholder="Ingrese la cantidad" value="{{ $product->quantity }}">
            </div>
            <div class="form-group mb-2 col-12">
                <label for="boarding_date">Fecha de embarque</label>
                <input type="date" name="boarding_date" class="form-control" value="{{ $product->boarding_date }}">
            </div>
            <div class="form-group mb-2 col-12">
                <label for="observations">Observaciones</label>
                <textarea name="observations" rows="5" cols="20" class="form-control" placeholder="Ingrese observaciones">{{ $product->observations }}</textarea>
            </div>
            <button class="btn btn-block btn-success mx-3">Actualizar</button>
        </form>
    </div>
@endsection
